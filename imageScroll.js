let index = 1;
let dynamicArray = [];
let url = '';
let decrem = false;
let myDom = '';
let domIndex = 1;
let displayFirst = '';
let buffer = false;
let forward = false;
let backward = true;
let readjust = true;
function pullImages() {
  $.ajax({
    type: "GET",
    async: true,
    dataType: 'json',
    url: "grabImages.php",
    success: function (urld) {
      displayUrl = urld;
      url = urld
      displayFirstImgs(displayUrl);
    }
  });
}

//MAIN PROBLEMS: ------------------------------------------------------------------------
//1.) Going backwards skips the first image after the starting point (the 150 x 150 img ) and the 
//2.) Takes an extra click for fourth image after going backwards past starting point (1700 x 1129) 
//other than this, everything else should work other than any minor bugs i haven't noticed.. 

//display the original 5 images (back two, first 3....)
function displayFirstImgs(url) {
  let first = url[0];
  let sec = url[1];
  let third = url[2];
  let fourth = url[3];
  let fifth = url[4];
  dynamicArray = [first, sec, third, fourth, fifth];
  //displaying first 5 images
  for (let i = 0; i <= 4; i++) {
    let tifSeeker = dynamicArray[i].lastIndexOf('tif');
    let tifSeekerCaps = dynamicArray[i].lastIndexOf('TIF');
    if (tifSeeker <= -1 && tifSeekerCaps <= -1) {
      //if image is not a tif file...
      newLoc = dynamicArray[i].replace('C:/inetpub/wwwroot/bradJS/imgScroll/', '');
      let urlVar = document.createElement('img');
      urlVar.setAttribute('class', 'mySlides');
      urlVar.setAttribute('src', newLoc);

      if (i === 0) {
        //display the first image
        urlVar.setAttribute('style', 'display: block;')
      }
      myDom = document.querySelector('.pasteData');
      myDom.append(urlVar);
    }
    //tif conversion (only converts if image is a tif)
    else {
      myDom = document.querySelector('.pasteData');
      let xhr = new XMLHttpRequest();
      xhr.responseType = 'arraybuffer';
      newLoc = dynamicArray[i].replace('C:/inetpub/wwwroot/bradJS/imgScroll/', '');
      xhr.open('GET', newLoc);
      xhr.onload = function (e) {
        let tiff = new Tiff({ buffer: xhr.response });
        let canvas = tiff.toCanvas();
        canvas.setAttribute('class', 'mySlides');
        if (i === 0) {
          canvas.setAttribute('style', 'display: block;')
        }
        myDom.append(canvas);
      };
      xhr.send();
    }
  }
  return dynamicArray;
}

function subtractNum(dynamicArray, url) {
  decrem = true;
  if (dynamicArray.includes(url[index]) === false) { //if not already in the array
    console.log('checking...')
    checkTif();
  }
  else{
    console.log('already present, skipping');
    showDivs(-1);
  }
}

//Triggered when user clicks right arrow....
function addNum(dynamicArray, url) {
  buffer = false;
  decrem = false;
  console.log(dynamicArray);
  console.log(url);
  console.log(url[index]);
  console.log(index);
  if (dynamicArray.includes(url[index]) === false) {
    console.log('not present')
    checkTif();
  }
  else {
    console.log('already present, skipping');
    showDivs(1)
  }
}
function checkTif() {//checking to see if image is a tif file
  let tifSeeker = url[index].lastIndexOf('tif');
  let tifSeekerCaps = url[index].lastIndexOf('TIF');
  if (tifSeeker <= -1 && tifSeekerCaps <= -1) { //adding tif files to dom....
    appendNonTif()
  }
  else {
    appendTif()
  }
}

function appendTif(reduction) {
  let myDom = document.querySelector('.pasteData');
  let xhr = new XMLHttpRequest();
  xhr.responseType = 'arraybuffer';
  newLoc = url[index].replace('C:/inetpub/wwwroot/bradJS/imgScroll/', '');
  console.log('onload function pt 1');
  xhr.onload = function (e) {
    let tiff = new Tiff({ buffer: xhr.response });
    let canvas = tiff.toCanvas();
    canvas.setAttribute('class', 'mySlides');
    if (decrem === true) { // if back button was pressed, prepend to top/front
      myDom.prepend(canvas);
      dynamicArray.push(url[index]);
      showDivs(-1)
    }
    else { // if forward button was pressed, append to bottom/back
      myDom.append(canvas);
      dynamicArray.push(url[index]);
      showDivs(1)
    }
  }
  xhr.open('GET', newLoc);
  xhr.send()
}

function appendNonTif() {
  newLoc = url[index].replace('C:/inetpub/wwwroot/bradJS/imgScroll/', '');
  let urlVar = document.createElement('img');
  urlVar.setAttribute('class', 'mySlides');
  urlVar.setAttribute('src', newLoc);
  let myDom = document.querySelector('.pasteData');
  if (decrem === true) {
    myDom.prepend(urlVar);
    dynamicArray.push(url[index]);
    console.log('pushing non-tif');
    console.log(index);
    showDivs(-1)
  }
  else {
    myDom.append(urlVar);
    dynamicArray.push(url[index]);
    console.log('pushing non-tif');
    console.log(index);
    showDivs(1)
  }
}
function plusDivs(n) {
    if (n === -1) { //if back button is pressed 
      index = index - 1; //decrement the array of images pulled via php
      if (index <= 0) {
        index = url.length - 1; //if the index goes past the beginning, move to back of array
      }
      domIndex = domIndex - 1; //decrement the dom index         
      if (domIndex <= 0) {
        domIndex = 0; //if the dom index goes past the beginning and there are still more items in the array, 
        //keep at 0 and prepend to top
      }
      subtractNum(dynamicArray, url);
    }
    else { //if forward button is pressed
      index = index + 1; // increment the array of images pulled via php 
      if (index >= url.length) {
        index = 0; //if index goes past the end, move to front of array
      }
      let domLength = document.getElementsByClassName("mySlides").length + 1;
      console.log(domLength);
      domIndex = domIndex + 1;
      if (domIndex >= domLength) {
        domIndex = 1; //Move dom index to front if you go past the end of the dom
        readjust = false //do not readjust indexes (prevents line 240 from being called...)
      }
      console.log('adding one to dom: ' + domIndex)
      addNum(dynamicArray, url)
    }
  }
  function showDivs(n) {
    let i;
    let currentDom = document.getElementsByClassName("mySlides");
    for (i = 0; i < currentDom.length; i++) {
      currentDom[i].style.display = "none";
    }
    //if going backwards 
    if (n === -1) {
      if (forward === true) { // if going forward -> backwards, adjust index accordingly
        domIndex = domIndex - 1;
        forward = false;
      } 
      backward = true;
      currentDom[domIndex].style.display = "block";
      console.log('displaying dom index ' + domIndex);
      console.log('displaying url index ' + index);
      console.log(currentDom[domIndex]);
      //if all of the elements have been appended and you're at the end (top), reset the dom index to the bottom
      if (domIndex <= 0 && document.getElementsByClassName("mySlides").length === url.length) {
        domIndex = document.getElementsByClassName("mySlides").length;
      }
    }
    // First time going forwards after having gone backwards
    else if (n === 1 && domIndex === 1 && index !== 2 && readjust === true) {
      buffer = true;
      plusDivs(1); //allows the indexes to adjust properly after going backwards -> forward
      console.log('back then forward dom ' + domIndex);
    }
    //going forward
    else {
      console.log('forward dom ' + domIndex);
      currentDom[domIndex - 1].style.display = "block";
      forward = true;
    }
  }
  pullImages();